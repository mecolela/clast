# Clast 

> Experimental

A universal javascript meta-framework based on vue, geared towards a
modular data store setup while offering optional PWA support and a
backend powered by the [Plade](https://plade.io) platform.

### Motivations

- [Rendering on the web, a modern day approach](https://youtu.be/k-A2VfuUROg)
- [Drastically reduce input latency & time to interactive of SSR apps](https://markus.oberlehner.net/blog/how-to-drastically-reduce-estimated-input-latency-and-time-to-interactive-of-ssr-vue-applications/)

## Getting started

**Configure your preferences and credentials in the
``config/local.json`` file**
[Read more about the config file setup here](https://github.com/lorenwest/node-config/wiki/Configuration-Files)

install necessary dependencies with [yarn](https://yarnpkg.com/en/)
```bash
yarn
```

build the base project to get a few things rolling
```bash
yarn build
```

if enabled in the configuration file (``sw.enabled``), build out the service-worker with
```bash
yarn build:sw
```

run the development server
> might require the backend api to be running to complete requests if
> you are using prebuilt store modules
```bash
yarn dev
```
if developing the service-worker run
```
yarn dev:sw
```

to start the server in prod mode
```bash
yarn start
```

### Caching

To optimize the renderer's compile & response time, you will need to
pass in a serverCacheKey under dynamic components.
[Read More here.](https://ssr.vuejs.org/guide/caching.html#component-level-caching)

You can additionally add a Redis cache layer to respond with already
rendered content. Simply enable output caching under the server config
property then configure the appropriate endpoints for the redis data
store under the redis configuration property.

### Documentation

A clear and concise document about the inner workings of the framework are in the works,
so for now just know the ``src`` directory is where you put your stuff

### TODO

As the project continues to mature, a few more optimizations and
enhancement need to be done

- ~~Router & store module Code splitting~~
- ~~Better sub component swap outs on navigation after code splitting~~
- ~~Offline resilience~~
- ~~Optimize output in production builds~~
- ~~Add support for lazy (sly) hydration~~
- Implement project scaffolding
- Support executable config file (``clast.config.js``)
- Optimize the pre-cache manifest and initial cache size
- Support dynamic store module loading on component create 
- better error handling for async data fetches, the goal:
```javascript
asyncData({store, route, context, error}) {
   error({statusCode: 404, message: 'Item not found'})
}
```

### Why the name

Taking inspiration from [clastic sedimentary rocks](https://g.co/kgs/a2M9jz) composed of fragments,
or clasts of pre-existing minerals and rocks. Clast strives to be 
modular and extendable around application data, while still staying afloat modern web development trends.
