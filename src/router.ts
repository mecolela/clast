import PageNotFound from '@/pages/errors/404.vue'
import Error from '@/pages/errors/500.vue'
import RootState from '@/store/RootState'
import {RouterOptions, VueRouter} from 'vue-router/types/router'
import {Store} from 'vuex'

const Home = () => import( /* webpackChunkName: "home" */ '@/pages/Index.vue')

const routerConfig: RouterOptions = {
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    // These are required components
    {
      path: '*',
      redirect: '/not-found'
    },
    {
      path: '/not-found',
      name: 'not-found',
      component: PageNotFound
    },
    {
      path: '/error',
      name: 'error',
      component: Error
    }
  ]
}

function routerHooks(router: VueRouter, store: Store<RootState>) {
  router.beforeEach((to, from, next) => {
    // if (to.matched.some(route => route.meta.requiresAuth)) {}
    next()
  })
}

export {routerConfig as default, routerHooks}
