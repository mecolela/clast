import catalogue from '@clast/catalogue-mod'
import cart from '@clast/cart-mod'
import user from '@clast/user-mod'
import order from '@clast/order-mod'
import notification from '@clast/notification-mod'
import ui from './modules/ui'

export default {
  modules: [
    ui,
    catalogue,
    user,
    cart,
    order,
    notification
  ]
}
