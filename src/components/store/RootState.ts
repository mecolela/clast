import {InitialState} from '@clast/store'
import {CartState} from '@clast/cart-mod'
import {ProductState, CollectionState, CategoryState} from '@clast/catalogue-mod'
import {OrderState} from '@clast/order-mod'
import {UserState} from '@clast/user-mod'
import {UiState} from './modules/ui'
import {NotificationState} from '@clast/notification-mod'

export default interface RootState extends InitialState {
  ui: UiState
  cart: CartState
  order: OrderState
  user: UserState
  products: ProductState
  collections: CollectionState
  notification: NotificationState
  categories: CategoryState
}
