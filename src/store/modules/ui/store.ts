import {Module} from 'vuex'
import RootState from '../../RootState'
import {UiState} from './'
import * as types from './types'

const module: Module<UiState, RootState> = {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {
    [types.GENERAL_SET](state, {key, value}) {
      state[key] = value
    }
  }
}

export default module
