const showNotification = (title, options) => Notification.permission === 'granted' ? self.registration.showNotification(title, options) : null

export default (ev) => {
  if (!ev.data || (ev.data.json && !isJson(ev.data))) return

  const data = ev.data.json()
  if (!data.type || !data.content) return
  const {type, content} = data

  switch (type) {
    case 'notification':
      if (content && content.body && content.title) {
        const {title, ...options} = content
        ev.waitUntil(showNotification(title, options))
      } else {
        console.error('Invalid Notification: A body and title are required.')
      }
      break;
    default:
      console.debug(`Unsupported message type ${data.type}`)
  }
}

const isJson = (data) => {
  let isIt = true
  try {
    data.json()
  } catch (e) {
    isIt = false
  }
  return isIt
}
