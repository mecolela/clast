import * as localForage from 'localforage'
import config from 'config'

const iDb = (storeName) => localForage.createInstance({
  storeName,
  name: config.runtime.storage.dbName,
  driver: localForage.INDEXEDDB
})

export default async (ev) => {
  const q = await ev.queue.getAll()
  const cartDb = iDb('cart')
  let cH
  if ('BroadcastChannel' in self) {
    cH = new BroadcastChannel('sw:messages')
  }

  const actions = q.map(() => async () => {
    const queueItem = await ev.queue.popRequest()
    const {pathname} = new URL(queueItem.request.url)
    const service = pathname.split('/')[2]
    console.debug('Syncing:', service, 'service')

    switch (service) {
      case 'carts':
        const state = await cartDb.getItem('state')
        if (!state) return

        // Passing over DELETE requests with a content-type causes an error
        const req = new Request(queueItem.request)
        if (req.method === 'DELETE') {
          req.headers.delete('content-type')
        }

        try {
          const res = await fetch(req).then(y => y.json())
          if (res.status < 400) {
            state.local = res.data
            await cartDb.setItem('state', state)
            cH && cH.postMessage({service, isSync: true, success: true})
            console.debug(`Successfully Synced: ${queueItem.request.url}`)
          } else {
            cH && cH.postMessage({
              service, isSync: true, success: false,
              fail: {op: queueItem.request.method, path: pathname, error: res.message}
            })
            console.debug('Sync failed:', res.message)
          }
        } catch (error) {
          cH && cH.postMessage({
            service, isSync: true, success: false,
            fail: {op: queueItem.request.method, path: pathname, error}
          })
          q.unshiftRequest({request: queueItem.request, timestamp: Date.now(), metadata: queueItem.metadata})
          console.debug('Sync failed:', error)
        }
        break
      case 'orders':
        console.debug('Service yet to implemented')
        break
      case 'customers':
        console.debug('Service yet to implemented')
        break
      default:
        console.debug('Service yet to implemented')
    }
  })

  actions.reduce((promise, func) => promise.then(() => func()), Promise.resolve())
}
