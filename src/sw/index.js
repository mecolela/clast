import config from 'config'
import {MessageHandler, NotificationHandler, SyncHandler} from './handlers'

self.addEventListener('install', () => self.skipWaiting())

self.addEventListener('push', MessageHandler)
self.addEventListener('message', MessageHandler)
self.addEventListener('notificationclick', NotificationHandler)

/* WORKBOX STUFF */

if (workbox) {
  const dbName = config.runtime.storage.dbName
  workbox.setConfig({debug: false})
  workbox.core.setCacheNameDetails({prefix: dbName, suffix: false})

  workbox.precaching.precacheAndRoute(self.__precacheManifest)
  workbox.core.skipWaiting()
  workbox.core.clientsClaim()

  workbox.routing.setCatchHandler(err => console.debug(err.message || err.err.message || err))
  workbox.routing.setDefaultHandler(new workbox.strategies.StaleWhileRevalidate())

  // Cache any images for 20 days before expiring them
  workbox.routing.registerRoute(/\.(?:jpeg|jpg|gif|png|svg|woff|woff2)/g,
    new workbox.strategies.CacheFirst({
      cacheName: `${dbName}-static`,
      plugins: [
        new workbox.expiration.Plugin({
          maxEntries: 60,
          maxAgeSeconds: 20 * 24 * 60 * 60
        })
      ]
    }))

  /* BACKGROUND SYNC */

  const bgSyncPlugin = new workbox.backgroundSync.Plugin(`${dbName}-queue`, {onSync: SyncHandler})

  // Handle failed PUT requests
  workbox.routing.registerRoute(({url}) => url.host === config.sw.backendHost,
    new workbox.strategies.NetworkOnly({plugins: [bgSyncPlugin]}), 'PUT')

  // Handle failed POST requests
  workbox.routing.registerRoute(({url}) => url.host === config.sw.backendHost,
    new workbox.strategies.NetworkOnly({plugins: [bgSyncPlugin]}), 'POST')

  // Handle failed DELETE requests
  workbox.routing.registerRoute(({url}) => url.host === config.sw.backendHost,
    new workbox.strategies.NetworkOnly({plugins: [bgSyncPlugin]}), 'DELETE')
} else {
  console.debug(`Workbox failed to load 😬`)
}
