import { ClastApp } from '@clast/core';
import EventBus from '@clast/plugins/event-bus';
import VeeValidate from 'vee-validate';
import { capitalize, date, price } from '@clast/filters';
import { routerHooks } from './router';
import GlobalMixin from './global-mixin';
import NotSsr from 'vue-no-ssr';

export default function ({Vue, router, store, ssrContext}: ClastApp) {
  Vue.use(EventBus);
  Vue.use(VeeValidate);
  Vue.mixin(GlobalMixin);
  Vue.component('not-ssr', NotSsr);
  Vue.filter('capitalize', capitalize);
  Vue.filter('date', date);
  Vue.filter('price', price);
  routerHooks(router, store);

  // Not on the server
  if(!ssrContext) {
    window.addEventListener('load', () => {
      if (!(navigator as any).standalone || !matchMedia('(display-mode: standalone)').matches) {
        // Launched in a Browser Tab, therefore can prompt an install
        window.addEventListener('beforeinstallprompt', (e) => {
          // Prevent the mini-infobar from appearing on mobile
          e.preventDefault();
          // Stash the event so it can be triggered later in the app.
          store.dispatch('ui/preparePrompt', e);
        });
      }
    });
  }
}
