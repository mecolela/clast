const fs = require('fs')
const config = require('config')
const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('./webpack.base.config')
const WorkboxPlugin = require('workbox-webpack-plugin')

const swPath = './src/sw/index.js'
if (!fs.existsSync(swPath) || !config.sw.enabled) {
  console.log('\nError: Service worker not found or is not enabled\n')
  process.exit(0)
}

module.exports = merge(base, {
  mode: 'production',
  target: 'web',
  entry: swPath,
  output: {
    filename: 'service-worker.js'
  },
  plugins: [
    new WorkboxPlugin.InjectManifest({
      swSrc: 'service-worker.js'
    }),
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': '"client"'
    })
  ]
})
