const isLocal = !process.env.RUN_ENV || process.env.RUN_ENV !== 'BUILD'
const {exec} = require('child_process')

async function build() {
  console.log('Please wait, making an initial build...')
  await exec('yarn build')
}

if (isLocal) {
  build()
}
