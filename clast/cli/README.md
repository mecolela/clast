# @clast/cli

## Installation
```
npm i -g @clast/cli
# OR
yarn global add @clast/cli
```

### Initialize

```bash
# create a new clast project
clast init my-project [./path-to-directory]
```

### Project Development

```bash
# create initial build
clast build

# run development clast server
clast dev
```

### Production
```bash
# build clast
clast build

# run in production mode
clast start
```
