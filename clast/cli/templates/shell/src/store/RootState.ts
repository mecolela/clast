import {InitialState} from '@clast/store'
import {ProductState, CollectionState, CategoryState} from '@clast/catalogue-mod'
import {UiState} from './modules/ui'

export default interface RootState extends InitialState {
  ui: UiState
  products: ProductState
  collections: CollectionState
  categories: CategoryState
}
