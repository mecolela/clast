import {Module} from 'vuex'
import RootState from '../../RootState'
import {UiState} from './index'
import * as types from './types'

const module: Module<UiState, RootState> = {
  namespaced: true,
  state: {
    isMobile: false
  },
  getters: {
    isMobile: ({isMobile}) => isMobile
  },
  actions: {
    setIsMobile({commit}, value) {
      commit(types.GENERAL_SET, {key: 'isMobile', value})
    }
  },
  mutations: {
    [types.GENERAL_SET](state, {key, value}) {
      state[key] = value
    }
  }
}

export default module
