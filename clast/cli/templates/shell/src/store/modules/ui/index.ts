import {createStoreModule} from '@clast/store/lib'
import module from './store'

export default createStoreModule({
  store: {modules: [{key: 'ui', module}]}
})

export interface UiState {
  isMobile: boolean
}
