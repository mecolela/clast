import catalogue from '@clast/catalogue-mod'
import ui from './modules/ui'

export default {
  modules: [
    ui,
    catalogue
  ]
}
