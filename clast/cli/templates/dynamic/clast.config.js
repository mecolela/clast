module.exports = {
  entryElement: 'main',
  webpack(config) {
    return config
  },
  head: {
    title: '<%= name %>',
    titleTemplate: '%s | Simple & cool all through',
    htmlAttrs: {lang: 'en'},
    link: [
      {rel: 'favicon', href: '/favicon.ico'},
      {rel: 'manifest', href: '/manifest.json'}
    ],
    meta: []
  },
  modules: {
    cart: {
      taxPercent: .16,
      countPerItemQuantity: false
    },
    orders: {
      offline: {
        notification: {
          enabled: true,
          title: 'Order waiting!',
          message: 'Click here to confirm the order you made offline.',
          icon: '/assets/logo.png'
        }
      }
    }
  },
  extensions: {
    googleAnalytics: {
      id: ''
    }
  },
  redis: {
    keyPrefix: '<%= name %>',
    host: 'localhost',
    port: 6379, db: 0
  },
  server: {
    host: 'localhost',
    logIP: false,
    port: 4000,
    useOutputCache: false,
    outputCacheTtl: 86400
  },
  plade: {
    host: 'localhost:3000',
    client_id: 'WCX4xWDcrpflszD0RTr1sncxF66TlDYSZOaWn2PE5K',
    currency: 'KSH',
    version: 'v1',
    protocol: 'http'
  },
  runtime: {
    enableSw: true,
    backendSync: true,
    storage: {
      dbName: '<%= name %>',
      drivers: {
        user: 'LOCALSTORAGE',
        cart: 'LOCALSTORAGE',
        orders: 'LOCALSTORAGE',
        categories: 'INDEXEDDB',
        products: 'INDEXEDDB',
        cms: 'INDEXEDDB'
      }
    }
  }
}
