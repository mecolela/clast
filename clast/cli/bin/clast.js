#!/usr/bin/env node

const program = require('commander')
const chalk = require('chalk')
const didYouMean = require('didyoumean')
const Service = require('../lib')

program
  .version(require('../package').version)
  .usage('<cmd> [options]')

program
  .command('init <project-name>')
  .description('Create a new clast project')
  .action((name, cmd) => {
    const options = cleanArgs(cmd)
    const service = new Service(name)
    service.init(options)
  })

program
  .command('build')
  .description('Compiles the application for production deployment')
  .action((cmd) => {
    const options = cleanArgs(cmd)
    const service = new Service()
    service.build(options)
  })

program
  .command('dev')
  .description('Start the application in development mode')
  .action((cmd) => {
    const options = cleanArgs(cmd)
    const service = new Service()
    service.dev(options)
  })

program
  .command('start')
  .description(`Start the application in production mode (Compile [${chalk.blue('clast build')}] first)`)
  .action((cmd) => {
    const options = cleanArgs(cmd)
    const service = new Service()
    service.start(options)
  })

program
  .arguments('<cmd>')
  .action((cmd) => {
    program.outputHelp()
    console.log(chalk.red(`  Unknown command ${chalk.blue(cmd)}`))
    suggestCommands(cmd)
  })

program.parse(process.argv)

if (!process.argv.slice(2).length) {
  program.outputHelp()
}

function suggestCommands(cmd) {
  const availableCommands = program.commands.map(cmd => cmd._name)
  const suggestion = didYouMean(cmd, availableCommands)
  if (suggestion) console.log(chalk.red(`  Did you mean ${chalk.green(suggestion)}?`))
}

function cleanArgs(cmd) {
  const args = {}
  cmd.options.forEach(o => {
    const key = camelize(o.long.replace(/^--/, ''))
    if (typeof cmd[key] !== 'function' && typeof cmd[key] !== 'undefined') {
      args[key] = cmd[key]
    }
  })
  return args
}
