import * as localForage from 'localforage'
import config from 'config'

type Driver = 'INDEXEDDB' | 'LOCALSTORAGE' | 'WEBSQL'

interface StorageDriver extends LocalForage {
}

function initStorage(key: string, driver?: Driver): StorageDriver {
  const storage = config.runtime.storage
  return localForage.createInstance({
    name: storage.dbName,
    storeName: key,
    driver: localForage[driver || storage.drivers[key]]
  })
}

export {StorageDriver, initStorage}
