import RootState from '@/store/RootState'
import Vue, {VueConstructor} from 'vue'
import Router, {NavigationGuard, Route, RouteConfig} from 'vue-router'
import {Module, Store} from 'vuex'
import {VueRouter} from 'vue-router/types/router'

interface IHookParams {
  Vue: VueConstructor
  config: any
  store: Store<RootState>
  router: VueRouter
  isServer: boolean
}

export interface StoreModuleConfig<State = any> {
  store?: {
    modules?: Array<{ key: string, module: Module<State, RootState> }>,
    plugins?: ((store: Store<any>) => any)[]
  }
  router?: {
    routes?: RouteConfig[],
    beforeEach?: NavigationGuard,
    afterEach?: (to?: Route, from?: Route) => any
  },
  beforeRegistration?: (hookParams: IHookParams) => void,
  afterRegistration?: (hookParams: IHookParams) => void
}

export class StoreModule {

  get config() {
    return this._config
  }

  private static registeredModules: StoreModuleConfig[] = []

  private static exists(key: string): boolean {
    let moduleExists = false
    StoreModule.registeredModules.forEach(module => {
      moduleExists = module.store && module.store.modules.some(m => m.key === key)
    })
    return moduleExists
  }

  private static extendStore(storeInstance: Store<any>, modules: Array<{ key: string, module: Module<any, any> }>, plugins: any[]): void {
    if (modules) modules.forEach(store => storeInstance.registerModule(store.key, store.module))
    if (plugins) plugins.forEach(plugin => plugin(storeInstance))
  }

  private static extendRouter(routerInstance: Router, routes?: RouteConfig[], beforeEach?: NavigationGuard, afterEach?: (to?: Route, from?: Route) => any): void {
    if (routes) routerInstance.addRoutes(routes)
    if (beforeEach) routerInstance.beforeEach(beforeEach)
    if (afterEach) routerInstance.afterEach(afterEach)
  }

  constructor(private _config: StoreModuleConfig) {
  }

  register(storeInstance: Store<any>, routerInstance?: Router): StoreModuleConfig | void {
    const c = this._config
    let isUnique = true
    if (c.store) {
      c.store.modules.forEach(store => {
        if (StoreModule.exists(store.key)) {
          console.error(`Error during module registration! Store with key "${store.key}" already exists!`)
          isUnique = false
        }
      })
    }

    if (isUnique) {
      const params: IHookParams = {
        Vue,
        config: storeInstance.state.config,
        store: storeInstance,
        router: routerInstance,
        isServer: Vue.prototype.$isServer
      }
      if (c.beforeRegistration) c.beforeRegistration(params)
      if (c.store) StoreModule.extendStore(storeInstance, c.store.modules, c.store.plugins)
      if (c.router) StoreModule.extendRouter(routerInstance, c.router.routes, c.router.beforeEach, c.router.afterEach)
      StoreModule.registeredModules.push(c)
      if (c.afterRegistration) c.afterRegistration(params)

      return c
    }
  }
}

export function createStoreModule<State = any>(config: StoreModuleConfig<State>): StoreModule {
  return new StoreModule(config)
}
