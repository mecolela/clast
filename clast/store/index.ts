import appStore from '@/store'
import RootState from '@/store/RootState'
import Vue from 'vue'
import Vuex, {Store} from 'vuex'
import {StoreModule} from './lib'
import ClastStore from './types/ClastStore'
import InitialState from './types/InitialState'
import {VueRouter} from 'vue-router/types/router'

function registerModules(modules: StoreModule[], store: Store<any>, router): void {
  modules.forEach(module => module.register(store, router))
}

Vue.use(Vuex)
const TS: ClastStore = Object.assign({}, appStore)

function createStore(config, router: VueRouter): Store<RootState> {
  Vue.prototype.$db = {}

  const store = new Vuex.Store<RootState>({
    plugins: [...(TS.plugins || [])]
  })

  store.state.config = config.runtime

  registerModules(TS.modules, store, router)
  return store
}

export {InitialState, createStore}
