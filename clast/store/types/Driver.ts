type DriverType = 'INDEXEDDB' | 'LOCALSTORAGE' | 'WEBSQL'

export default DriverType
