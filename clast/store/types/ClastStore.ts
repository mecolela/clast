import {StoreModule} from '@clast/store/lib'
import {Plugin} from 'vuex'
import RootState from '@/store/RootState'

export default interface ClastStore {
  modules: StoreModule[],
  plugins?: Plugin<RootState>[]
}
