import {createStoreModule} from '@clast/store/lib'
import module from './store'

export default createStoreModule({
  store: {
    modules: [{key: 'breadcrumbs', module}]
  }
})
