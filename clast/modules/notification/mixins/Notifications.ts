import {NotificationItem} from '../types/NotificationItem'

export const Notifications = {
  name: 'Notification',
  computed: {
    notifications(): NotificationItem[] {
      return this.$store.getters['notification/notifications']
    }
  }
}
