export interface PagedList {
  start: number
  perPage: number
  items: any[]
}

export default interface BaseState {
  current: any
  list: any[] | PagedList
  [field: string]: any
}
