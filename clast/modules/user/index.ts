import {createStoreModule} from '@clast/store/lib'
import {module} from './store'
import {initStorage} from '@clast/store/storage'
import plugin from './store/plugin'
import UserState from './types/UserState'

export default createStoreModule({
  store: {
    modules: [{key: 'user', module}],
    plugins: [plugin]
  },
  afterRegistration({store, isServer}) {
    if (!isServer) store.dispatch('user/load')
  },
  beforeRegistration({Vue, isServer}) {
    if (!isServer) Vue.prototype.$db.userCollection = initStorage('user')
  }
})

export {UserState}
