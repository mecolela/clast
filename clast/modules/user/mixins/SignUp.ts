export const SignUpMixin = {
  name: 'SignUp',
  data() {
    return {
      form: {
        email: '',
        name: '',
        password: ''
      }
    }
  },
  methods: {
    signUp() {
      return this.$store.dispatch('user/signUp', {
        email: this.form.email, password: this.form.password, name: this.form.name
      })
    }
  }
}

export default SignUpMixin
