import {formatType} from '@clast/store/lib/utils'
import Vue from 'vue'
import {Plugin} from 'vuex'
import * as types from './mutation-types'
import RootState from '@/store/RootState'

const reFormat = (type) => formatType(types.SN_USER, type)

export const UserLocalStore: Plugin<RootState> = store => {
  store.subscribe((mutation, state) => {
    switch (mutation.type) {
      case reFormat(types.USER_SET_TOKEN):
        Vue.prototype.$db.userCollection
          .setItem('token', state.user.token)
          .catch((e) => console.error('collection plugin error:', e.message || e))
        break
      case reFormat(types.USER_SET_CURRENT):
        Vue.prototype.$db.userCollection
          .setItem('current', state.user.current)
          .catch((e) => console.error('collection plugin error:', e.message || e))
        break
    }
  })
}

export default UserLocalStore
