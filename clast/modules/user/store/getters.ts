import RootState from '@/store/RootState';
import { GetterTree } from 'vuex';
import UserState from '../types/UserState';

const getters: GetterTree<UserState, RootState> = {
  isAuthenticated: ({current}) => current !== null,
  current: ({current}) => current,
  token: ({token}) => token
};

export default getters;
