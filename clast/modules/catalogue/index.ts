import {createStoreModule} from '@clast/store/lib'
import {initStorage} from '@clast/store/storage'
import collectionModule from './store/collection'
import productModule from './store/product'
import ProductState from './types/ProductState'
import CategoryState from './types/CategoryState'
import CollectionState from './types/CollectionState'
import SearchState from './types/SearchState'
import CataloguePlugin from './store/plugin'

export default createStoreModule({
  store: {
    modules: [
      {key: 'products', module: productModule},
      {key: 'collections', module: collectionModule}
    ],
    plugins: [CataloguePlugin]
  },
  beforeRegistration({Vue, isServer}) {
    if (!isServer) {
      Vue.prototype.$db.collections = initStorage('collections')
      Vue.prototype.$db.products = initStorage('products')
    }
  }
})

export {ProductState, CategoryState, CollectionState, SearchState}
