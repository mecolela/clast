import {BaseMutations} from '@clast/base-mod'
import ProductState from '../../types/ProductState'
import * as types from '../mutation-types'

export default BaseMutations<ProductState>({
  LOAD: types.PRODUCT_LOAD,
  SET_LIST: types.PRODUCT_SET_LIST,
  RESET_CURRENT: types.PRODUCT_RESET_CURRENT,
  SET_CURRENT: types.PRODUCT_SET_CURRENT
}, {
  [types.PRODUCT_SET_RELATED](state, related) {
    state.related = related
  }
})
