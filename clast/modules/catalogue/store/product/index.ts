import RootState from '@/store/RootState'
import {BaseState} from '@clast/base-mod'
import {Module} from 'vuex'
import ProductState from '../../types/ProductState'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

// TODO: cache each product to the indexed db / simply rely on the service-worker

const productModule: Module<ProductState, RootState> = {
  namespaced: true,
  state: BaseState({related: []}),
  getters,
  actions,
  mutations
}

export default productModule
