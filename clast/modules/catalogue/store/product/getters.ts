import {BaseGetters} from '@clast/base-mod'
import RootState from '@/store/RootState'
import ProductState from '../../types/ProductState'

export default BaseGetters<ProductState, RootState>()
