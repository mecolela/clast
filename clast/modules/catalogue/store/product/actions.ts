import {BaseActions} from '@clast/base-mod'
import Service from '@clast/service'
import ProductState from '../../types/ProductState'
import RootState from '@/store/RootState'
import {HttpError} from '@clast/utils/errors'
import * as types from '../mutation-types'

export default BaseActions<ProductState, RootState>({
  api: Service.Products,
  dbRef: 'products',
  types: {
    LOAD: types.PRODUCT_LOAD,
    SET_LIST: types.PRODUCT_SET_LIST,
    SET_CURRENT: types.PRODUCT_SET_CURRENT,
    RESET_CURRENT: types.PRODUCT_SET_CURRENT
  },
  extend: {
    fetchBySlug({commit, dispatch}, {slug, select = [], clear = false}) {
      if (clear) {
        commit(types.PRODUCT_SET_CURRENT, null)
      }
      return dispatch('fetchIndexed', {
        key: 'slug', value: slug, select
      }).then((data) => {
        const product = data
        if (product) {
          return product
        } else {
          throw new HttpError('Product Not Found', 404)
        }
      })
    }
  }
})
