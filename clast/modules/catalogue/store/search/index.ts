import RootState from '@/store/RootState'
import {Module} from 'vuex'
import SearchState from '../../types/SearchState'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const productModule: Module<SearchState, RootState> = {
  namespaced: true,
  state: {
    search: '',
    results: []
  },
  getters,
  actions,
  mutations
}

export default productModule
