import {BaseMutations} from '@clast/base-mod'
import CollectionState from '../../types/CollectionState'
import * as types from '../mutation-types'

export default BaseMutations<CollectionState>(
  {
    LOAD: types.COLLECTION_LOAD,
    RESET_CURRENT: types.COLLECTION_RESET_CURRENT,
    SET_CURRENT: types.COLLECTION_SET_CURRENT,
    SET_LIST: types.COLLECTION_SET_LIST
  }
)
