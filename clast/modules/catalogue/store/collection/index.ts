import {BaseState} from '@clast/base-mod'
import RootState from '@/store/RootState'
import {Module} from 'vuex'
import CollectionState from '../../types/CollectionState'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const collectionModule: Module<CollectionState, RootState> = {
  namespaced: true,
  state: BaseState<CollectionState>(),
  getters,
  actions,
  mutations
}

export default collectionModule
