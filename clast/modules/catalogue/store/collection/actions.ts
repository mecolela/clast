import {BaseActions} from '@clast/base-mod'
import Service from '@clast/service'
import RootState from '@/store/RootState'
import CollectionState from '../../types/CollectionState'
import * as types from '../mutation-types'
import {HttpError} from '@clast/utils/errors'

export default BaseActions<CollectionState, RootState>({
  api: Service.Collections,
  dbRef: 'collections',
  types: {
    LOAD: types.COLLECTION_LOAD,
    SET_LIST: types.COLLECTION_SET_LIST,
    SET_CURRENT: types.COLLECTION_SET_CURRENT,
    RESET_CURRENT: types.COLLECTION_RESET_CURRENT
  },
  extend: {
    fetchBySlug({commit, dispatch}, {slug, select = [], clear = false}) {
      if (clear) {
        commit(types.COLLECTION_SET_CURRENT, null)
      }
      return dispatch('fetchIndexed', {
        key: 'slug', value: slug, select
      }).then(async (data) => {
        const collection = data
        if (collection) {
          return collection
        } else {
          throw new HttpError('Collection Not Found', 404)
        }
      })
    }
  }
})
