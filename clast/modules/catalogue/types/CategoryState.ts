import BaseState from '@clast/base-mod/types/BaseState'

export default interface CategoryState extends BaseState {
  filters: {
    available: any,
    chosen: any
  },
  current_path: any[]
}
