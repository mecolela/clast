import {MutationTree} from 'vuex'
import CartState from '../types/CartState'
import * as types from './mutation-types'

const mutations: MutationTree<CartState> = {
  [types.CART_ADD_ITEM](state, item) {
    const record = state.local.items.find(p => p.id === item.id)
    if (!record) state.local.items.push(item)
  },
  [types.CART_DEL_ITEM](state, id) {
    state.local.items = state.local.items.filter(p => p.id !== id)
  },
  [types.CART_UPD_ITEM](state, item) {
    let record = state.local.items.find(p => (p.id === item.id))
    if (record) {
      let index = state.local.items.indexOf(record)
      state.local.items[index] = Object.assign(record, item)
    }
  },
  [types.CART_LOAD](state, cart) {
    state.coupon = !!cart.coupon ? cart.coupon : {code: '', discount: 0}
    state.local = !!cart.local ? cart.local : {
      items: cart.items,
      reference: cart.reference,
      totals: cart.totals,
      timestamps: {
        createdAt: cart.createdAt,
        updatedAt: cart.updatedAt
      }
    }
  },
  [types.CART_LOAD_REFERENCE](state, token) {
    state.local.reference = token
  },
  [types.CART_UPD_TOTALS](state, total) {
    state.local.totals = Object.assign(state.local.totals, total)
  },
  [types.CART_SET_COUPON](state, coupon) {
    state.coupon = coupon
  },
  [types.CART_SET_PAYMENT_METHODS](state, methods) {
    state.methods.payments = methods
  },
  [types.CART_SET_SHIPPING_METHODS](state, methods) {
    state.methods.shipping = methods
  },
  [types.CART_RESET](state) {
    state.local = {
      reference: '',
      items: [],
      totals: {
        tax: 0,
        shipping: 0,
        subTotal: 0,
        total: 0
      },
      timestamps: {
        createdAt: new Date(),
        updatedAt: new Date()
      }
    }
    state.coupon = {
      code: '',
      discount: 0
    }
  },
  [types.CART_CHECKOUT](state, value = true) {
    state.isCheckedOut = value
  }
}

export default mutations
