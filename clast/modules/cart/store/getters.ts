import {sumBy} from '@clast/utils'
import RootState from '@/store/RootState'
import {GetterTree} from 'vuex'
import CartState from '../types/CartState'

const getters: GetterTree<CartState, RootState> = {
  item: ({local}) => ({key, value}) => local.items ? local.items.find(p => p[key] === value) : [],
  items: ({local}) => local.items,
  totals: ({local}) => local.totals,
  coupon: ({coupon}) => coupon,
  totalQuantity: ({local}, _, {config}) => {
    if (config && config.cart.countPerItemQuantity) {
      return sumBy(local.items, (p) => p.quantity)
    } else {
      return local.items.length
    }
  },
  shippingMethods: ({methods}) => methods.shipping,
  paymentMethods: ({methods}) => methods.payments
}

export default getters
