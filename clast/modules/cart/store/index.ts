import {Module} from 'vuex'
import CartState from '../types/CartState'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'
import RootState from '@/store/RootState'

const module: Module<CartState, RootState> = {
  namespaced: true,
  state: {
    isCheckedOut: false,
    local: {
      reference: '',
      items: [],
      totals: {
        tax: 0,
        shipping: 0,
        subTotal: 0,
        total: 0
      },
      timestamps: {
        createdAt: new Date(),
        updatedAt: new Date()
      }
    },
    coupon: {
      code: '',
      discount: 0
    },
    methods: {
      shipping: [],
      payments: []
    }
  },
  getters,
  actions,
  mutations
}

export default module
