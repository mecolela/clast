export default interface Coupon {
  code: string
  discount: number
}
