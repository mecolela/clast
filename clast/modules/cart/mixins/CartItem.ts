import {mapGetters} from 'vuex'

export const CartItemMixin = {
  name: 'CartItem',
  props: {
    product: {
      type: Object,
      required: true
    }
  },
  computed: {
    ...mapGetters({
      cartItem: 'cart/item'
    }),
    item() {
      return this.cartItem({key: 'id', value: this.product.id})
    },
    count() {
      const it = this.item
      return it ? it.quantity : 0
    },
    inCart() {
      return !!this.item
    },
    stock() {
      if (this.product.stock && typeof this.product.stock === 'number') {
        return this.product.stock
      } else if (this.product.stock && this.product.stock.quantity) {
        return this.product.stock.quantity
      } else {
        return 0
      }
    },
    inStock() {
      if (this.product.stock && typeof this.product.stock === 'number') {
        return this.product.stock > 0
      } else if (this.product.stock && this.product.stock.quantity) {
        return this.product.stock.quantity > 0
      } else {
        return false
      }
    }
  },
  methods: {
    increment() {
      return this.$store.dispatch('cart/updateQuantity', {
        item: this.product, qty: this.count + 1
      })
    },
    decrement() {
      return this.count > 0 ? this.$store.dispatch('cart/updateQuantity', {
        item: this.product, qty: this.count - 1
      }) : console.error({code: 'ITEM_DOES_NOT_EXIST', message: 'Item does not exist in the cart'})
    },
    removeFromCart() {
      return this.$store.dispatch('cart/removeItem', this.product)
    }
  }
}

export default CartItemMixin
