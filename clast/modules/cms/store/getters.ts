import {GetterTree} from 'vuex'
import CmsState from '../types/CmsState'

const getters: GetterTree<CmsState, any> = {
  cmsData: ({data}) => data
}

export default getters
