import {createStoreModule} from '@clast/store/lib'
import module from './store'
import {initStorage} from '@clast/store/storage'
import CmsState from './types/CmsState'

export default createStoreModule({
  store: {modules: [{key: 'cms', module}]},
  beforeRegistration({Vue, isServer}) {
    if (!isServer) Vue.prototype.$db.cmsCollection = initStorage('cms')
  }
})

export {CmsState}
