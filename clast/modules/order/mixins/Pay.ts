export default {
  methods: {
    pay(gateway, method, data) {
      return this.$store.dispatch('order/payOrder', {
        gateway, method, data
      })
    }
  }
}
