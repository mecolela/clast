import RootState from '@/store/RootState'
import Service from '@clast/service'
import {isOnline} from '@clast/utils'
import Vue from 'vue'
import {ActionTree} from 'vuex'
import OrderState from '../types/OrderState'
import * as types from './mutation-types'

const orderApi = Service.Orders

const actions: ActionTree<OrderState, RootState> = {
  load({commit}) {
    return new Promise(((resolve, reject) => {
      if (Vue.prototype.$isServer) return
      const collection = Vue.prototype.$db.ordersCollection
      collection.getItem('current')
        .then((orders) => {
          if (orders) {
            commit(types.ORDER_LOAD, orders)
          }
          resolve()
        }).catch(reject)
    }))
  },
  async placeOrder({dispatch, commit, rootState}, order) {
    commit(types.ORDER_PLACE_ORDER, order)
    return order
  },
  updateOrder({commit, state}, update) {
    if (isOnline()) {
      orderApi.update(state.current.id, update)
        .then(({data: order}) => {
          commit(types.ORDER_SET_CURRENT, order)
        })
    } else {
      // Locally update hopping all goes well TODO: review
      commit(types.ORDER_UPDATE_CURRENT, update)
    }
  },
  payOrder({commit, state, rootGetters}, {gateway = 'mpesa', method = 'authorize', data}) {
    // can only pay for orders if is actually online
    if (!isOnline()) throw 'Currently offline'
    return orderApi.payment(state.current.id, {gateway, method, ...data}, rootGetters['user/token'])
  }
}

export default actions
