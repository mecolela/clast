import RootState from '@/store/RootState'
import Vue from 'vue'
import {Plugin} from 'vuex'
import * as types from './mutation-types'

export const OrderPlugin: Plugin<RootState> = store => {
  store.subscribe((mutation, state) => {
    if (Vue.prototype.$isServer) return
    if (mutation.type == types.ORDER_PLACE_ORDER) {
      Vue.prototype.$db.ordersCollection
        .setItem('current', state.order.current)
        .catch((e) => console.error('collection plugin error:', e.message || e))
    }
  })
}

export default OrderPlugin
