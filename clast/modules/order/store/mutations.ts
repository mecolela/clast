import {MutationTree} from 'vuex'
import OrderState from '../types/OrderState'
import * as types from './mutation-types'

const mutations: MutationTree<OrderState> = {
  [types.ORDER_LOAD](state, load) {
    state = load
  },
  [types.ORDER_PLACE_ORDER](state, order) {
    // order.timestamps.createdAt = new Date()
    // order.timestamps.updatedAt = new Date()

    state.current = order
  },
  [types.ORDER_UPDATE_CURRENT](state, update) {
    state.current = Object.assign(state.current, update)
    state.current.timestamps.updatedAt = new Date()
  },
  [types.ORDER_CANCEL_CURRENT](state) {
    // TODO: Plugin listen for mutation for cancellation
  }
}

export default mutations
