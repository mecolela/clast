import RootState from '@/store/RootState'
import {Module} from 'vuex'
import OrderState from '../types/OrderState'
import actions from './actions'
import mutations from './mutations'

export const module: Module<OrderState, RootState> = {
  namespaced: true,
  state: {
    current: null
  },
  actions,
  mutations
}
