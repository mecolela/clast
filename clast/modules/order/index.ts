import {createStoreModule} from '@clast/store/lib'
import {module} from './store'
import OrderPlugin from './store/plugin'
import {initStorage} from '@clast/store/storage'
import OrderState from './types/OrderState'

export default createStoreModule({
  store: {
    modules: [{key: 'order', module}],
    plugins: [OrderPlugin]
  },
  afterRegistration({store, isServer}) {
    if (!isServer) {
      store.dispatch('order/load')
      if (typeof window !== 'undefined') {
        // window.addEventListener('online', (ev) => store.dispatch('order/onlineHandler', ev))
      }
    }
  },
  beforeRegistration({Vue, isServer}) {
    if (!isServer) Vue.prototype.$db.ordersCollection = initStorage('orders')
  }
})

export {OrderState}
