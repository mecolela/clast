import Vue, {PluginObject} from 'vue'

export const EventBus = new Vue()

interface EvBusOption {
  propName?: string
}

export const EventBusPlugin: PluginObject<EvBusOption> = {
  install(Vue, options: EvBusOption = {propName: '$bus'}) {
    if (!Vue.prototype.$bus) {
      Object.defineProperties(Vue.prototype, {
        [options.propName]: {
          get: () => EventBus
        }
      })
    }
  }
}
export default EventBusPlugin
