import routerOptions from '@/router'
import Vue from 'vue'
import Router from 'vue-router'
import {VueRouter} from 'vue-router/types/router'

Vue.use(Router)

export const createRouter = (): VueRouter => new Router(Object.assign({
  mode: 'history',
  scrollBehavior: () => ({x: 0, y: 0}) // always reset to the top
}, routerOptions))
