import {createApp} from '..'
import config from 'config'

const {app, store, router} = createApp(null, config)

// register service-worker
if (config.sw.enabled) require('../app/sw')

declare const window: any
if (window.__CLAST_STATE__) {
  store.replaceState(window.__CLAST_STATE__)
}

function hydrateSubcomponents(components: any[], to) {
  return Promise.all(components.map(SubComponent => {
    if (SubComponent.asyncData) {
      return Promise.resolve(SubComponent.asyncData({store, route: to}))
    } else {
      return Promise.resolve(null)
    }
  }))
}

// TODO: improve dynamic error handling
router.beforeResolve((to, from, next) => {
  const matched = router.getMatchedComponents(to)

  if (!matched.length) return next()

  return Promise.all(matched.map((main: any) => {
    const subs = main.mixins ? Array.from(main.mixins) : []

    if (main.asyncData) {
      return Promise.resolve(main.asyncData({store, route: to}))
        .then(() => hydrateSubcomponents(subs, to))
    } else {
      return hydrateSubcomponents(subs, to)
    }
  })).then(() => {
    next()
  }).catch(e => {
    console.error(e)
    next({path: '/error'})
  })
})

router.onReady(() => {
  app.$mount(config.entryElement)
}, console.error)
