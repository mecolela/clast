import {createApp} from '..'

function entryErrorHandler(err, reject?) {
  if (reject) {
    reject(err)
  } else {
    throw err
  }
}

// TODO: improve custom error handling
function hydrateSubcomponents(components, store, router, context, app) {
  return Promise.all(components.map(component => {
    if (component.asyncData) {
      return Promise.resolve(component.asyncData({
        store, route: router.currentRoute,
        context, error: entryErrorHandler
      }))
    } else {
      return Promise.resolve(null)
    }
  }))
}

export default context => {
  return new Promise((resolve, reject) => {
    const {app, router, store} = createApp(context, context.clast.config)

    context.meta = (app as any).$meta()
    router.push(context.url)

    router.onReady((cb) => {
      const matched = router.getMatchedComponents()

      if (!matched.length) {
        reject({code: 404})
      }
      Promise.all(matched.map((main: any) => {
        const subs = main.mixins ? Array.from(main.mixins) : []

        if (main.asyncData) {
          return Promise.resolve(main.asyncData({
            store,
            route: router.currentRoute,
            context,
            error: entryErrorHandler
          }))
            .then(() => hydrateSubcomponents(subs, store, router, context, app))
            .catch((err) => entryErrorHandler(err, reject))
        } else {
          return hydrateSubcomponents(subs, store, router, context, app)
        }
      })).then(() => {
        context.state = store.state
        resolve(app)
      }).catch(reject)
    }, reject)
  })
}
