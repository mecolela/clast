const merge = require('webpack-merge')
const baseServerConfig = require('../dev/server.config')

module.exports = merge(baseServerConfig, {mode: 'production', devtool: false})
