const merge = require('webpack-merge')
const baseClientConfig = require('../dev/client.config')

module.exports = merge(baseClientConfig, {mode: 'production', devtool: false})
