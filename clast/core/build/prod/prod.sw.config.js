const webpack = require('webpack')
const merge = require('webpack-merge')
const base = require('../base.config')
const WorkboxPlugin = require('workbox-webpack-plugin')

module.exports = merge(base, {
  mode: 'production',
  target: 'web',
  entry: './src/sw/index.js',
  output: {
    filename: 'core-service-worker.js'
  },
  plugins: [
    new WorkboxPlugin.InjectManifest({
      swSrc: './src/sw/index.js',
      swDest: 'service-worker.js'
    }),
    new webpack.DefinePlugin({
      'process.env.VUE_ENV': '"client"'
    })
  ]
})
