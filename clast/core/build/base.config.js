const path = require('path')
const config = require('config')
const fs = require('fs')
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const autoprefixer = require('autoprefixer')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const isProd = process.env.NODE_ENV === 'production'
const cwd = process.cwd()
const cwdResolve = (p) => path.resolve(cwd, p)

if (!fs.existsSync('./.clast')) {
  fs.mkdirSync('./.clast')
}

fs.writeFileSync(
  cwdResolve('./.clast/config.json'),
  JSON.stringify(config)
)

const appRoot = './src'
const middleware = './src/middleware'
const indexTemplate = './public/index.template.html'

const appStatic = './public'
const appAssets = appRoot + '/assets'

const postcssConfig = {
  loader: 'postcss-loader',
  options: {
    ident: 'postcss',
    plugins: () => [
      require('postcss-flexbugs-fixes'),
      require('autoprefixer')({
        flexbox: 'no-2009'
      })
    ]
  }
}

module.exports = {
  node: {fs: 'empty'},
  performance: {
    maxEntrypointSize: 350000
  },
  plugins: [
    // new BundleAnalyzerPlugin({openAnalyzer: false}),
    new webpack.ProgressPlugin((percentage) => {
      if (!isProd) process.stdout.write(`Compiling: ${parseInt(percentage * 100, 10)}%\r`)
    }),
    new CaseSensitivePathsPlugin(),
    new CopyPlugin([
      {from: appStatic, to: 'public', ignore: ['*.js']},
      {from: appAssets, to: 'assets', ignore: ['*.js']},
      ...(fs.existsSync(middleware) && fs.existsSync(middleware + '/index.js')) ? [{
        from: middleware,
        to: 'middleware'
      }] : []
    ]),
    new VueLoaderPlugin(),
    new HTMLPlugin({
      template: indexTemplate,
      filename: 'index.html',
      inject: isProd === false,
      minify: {
        collapseWhitespace: true,
        useShortDoctype: true
      }
      // in dev mode we're not using clientManifest therefore renderScripts() is returning empty string and we need to inject scripts using HTMLPlugin
    })
  ],
  devtool: 'source-map',
  entry: {
    app: '@clast/core/entry/client.ts'
  },
  output: {
    path: cwdResolve('./dist'),
    publicPath: '/dist/',
    filename: '[name].[hash].js'
  },
  resolveLoader: {
    modules: [
      'node_modules',
      cwdResolve(appRoot)
    ]
  },
  resolve: {
    modules: [
      'node_modules',
      cwdResolve(appRoot)
    ],
    extensions: ['.js', '.vue', '.ts'],
    alias: {
      '@': cwdResolve('./src'),
      'config': cwdResolve('./.clast/config.json')
    }
  },
  module: {
    exprContextCritical: false,
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          preserveWhitespace: false,
          postcss: [autoprefixer()]
        }
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        options: {
          appendTsSuffixTo: [/\.vue$/]
        },
        include: [
          cwdResolve('./node_modules/@clast'),
          cwdResolve(appRoot)
        ]
        // exclude: /node_modules/
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          cwdResolve('./node_modules/@clast'),
          cwdResolve(appRoot)
        ],
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig
        ]
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig,
          'sass-loader'
        ]
      },
      {
        test: /\.sass$/,
        use: [
          'vue-style-loader',
          'css-loader',
          postcssConfig,
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: true
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)(\?.*$|$)/,
        loader: 'url-loader?importLoaders=1&limit=10000'
      }
    ]
  }
}
