/**
 * Converts date to format provided as an argument or defined in config file (if argument not provided)
 * @param {String} date
 * @param {String} locale
 */
export const date = (date: any, locale = 'en') => {
  return new Date(date).toLocaleString(locale)
}

export default date
