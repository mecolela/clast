/**
 * Trim text to specific length and add ellipses
 * @param {String} text
 * @param {Number} max
 */
export const truncate = (text: string, max: number) => {
  if (!text) return ''
  return (text.length > max) ? text.substr(0, max - 1) + '...' : text
}

export default truncate
