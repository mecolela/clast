/**
 * Strip HTML tags
 * @param {string} html
 */
export const stripHtml = (html: string) => {
  if (!html) return ''
  return html.replace(/<[^>]+>/g, '').trim()
}

export default stripHtml
