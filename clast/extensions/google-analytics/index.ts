import VueAnalytics from 'vue-analytics'
import {createStoreModule} from '@clast/store/lib'

export default createStoreModule({
    store: {
      modules: [{
        key: 'google-analytics', module: {
          namespaced: true
        }
      }]
    },
    beforeRegistration({Vue, config, router, isServer}) {
      if (config.analytics.id && !isServer) {
        Vue.use(VueAnalytics, {
          id: config.analytics.id, router,
          ecommerce: {enabled: true, enhanced: true}
        })
      } else {
        console.warn('Google Analytics extensions is not working. Ensure Google Analytics account ID is defined in the config file')
      }
    },
    afterRegistration({Vue, config, store, isServer}) {
      if (config.analytics.id && !isServer) {
        store.subscribe((mutation, state) => {
          if (mutation.type === 'order/SET_CURRENT') {
            const order = state.order.current
            const ecommerce = (Vue as any).$ga.ecommerce
            order.items.forEach(item => {
              ecommerce.addItem({
                id: item.id,
                name: item.name,
                sku: item.sku,
                price: item.unitPrice.withTax.toString(),
                quantity: item.quantity.toString()
              })
            })
            ecommerce.send()
          }
        })
      }
    }
  }
)
