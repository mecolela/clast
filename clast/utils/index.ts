export function isOnline() {
  if (typeof navigator !== 'undefined') {
    return navigator.onLine
  } else {
    return true
  }
}

export function sumBy(array: any[], iterator: (item: any) => any) {
  let val = 0
  array.forEach((item) => val += iterator(item))
  return val
}
