export class HttpError extends Error {
  code
  message
  meta

  constructor(message?: string, code = 500, meta = null) {
    message = message ? message : 'A server issue occurred while running your request'
    super(message)

    this.code = code
    this.message = message
    this.meta = meta
  }
}
